#!/bin/bash

cat >> ~/.bashrc << eol

# adding addtional aliases
alias upg='sudo apt update && sudo apt upgrade -y'
alias davaj='sudo apt install'
alias peng='sudo hping3 -S -p 80'
alias up='sudo apt update'
alias list='apt list --upgradable'
alias refersh='kquitapp5 plasmashell && kstart5 plasmashell'

eol
